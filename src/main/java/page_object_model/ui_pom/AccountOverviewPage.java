package page_object_model.ui_pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountOverviewPage {


    private WebDriver driver;


    public static String accountOverviewURL = "https://parabank.parasoft.com/parabank/overview.htm";
    public By logoutButton = By.xpath("//div[@id='leftPanel']/ul/li[8]/a");
    public By accountNumber = By.xpath("//table[@id='accountTable']//td[1]/a");
    public By firstBalance = By.xpath("//table[@id='accountTable']//td[2]");
    public By firstAvailableAmount = By.xpath("//table[@id='accountTable']//td[3]");
    public By firtTotal = By.xpath("//table[@id='accountTable']//tbody/tr[2]/td[2]/b");

    public AccountOverviewPage(WebDriver driver) {
        this.driver = driver;
    }


    public String getAccountOverviewURl() {
        return driver.getCurrentUrl();
    }

    public boolean logoutButtonIsDisplayed() {
        return driver.findElement(logoutButton).isDisplayed();
    }
    public String getAccountNumber() {
        return driver.findElement(accountNumber).getText();
    }
    public String getBalance() {
        return driver.findElement(firstBalance).getText();
    }
    public String getAvailableAmount() {
        return driver.findElement(firstAvailableAmount).getText();
    }
    public String getTotal() {
        return driver.findElement(firtTotal).getText();
    }
}
