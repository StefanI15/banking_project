package page_object_model.ui_pom;

import com.github.javafaker.Faker;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterPage {

    private final WebDriver driver;

    private String baseUsername = "Clay";
    private Faker faker = new Faker();
    private String dynamicUserName;

    public static String registerURL = "https://parabank.parasoft.com/parabank/register.htm";

    public By firstName = By.cssSelector("input[name='customer.firstName']");
    public By lastName = By.cssSelector("input[name='customer.lastName']");
    public By address = By.cssSelector("input[name='customer.address.street']");
    public By city = By.xpath("//*[@id='customer.address.city']");
    public By state = By.xpath("//*[@id='customer.address.state']");
    public By zipCode = By.xpath("//*[@id='customer.address.zipCode']");
    public By phone = By.xpath("//*[@id='customer.phoneNumber']");
    public By securityNumber = By.xpath("//*[@id='customer.ssn']");
    public By userName = By.xpath("//*[@id='customer.username']");
    public By password = By.xpath("//*[@id='customer.password']");
    public By passwordConfirmation = By.xpath("//*[@id='repeatedPassword']");

    public By register = By.xpath("//*[@value='Register']");
    public By firsNameError = By.xpath("//span[@id='customer.firstName.errors']");
    public By lastNameError = By.xpath("//span[@id='customer.lastName.errors']");
    public By addressError = By.xpath("//span[@id='customer.address.street.errors']");
    public By cityError = By.xpath("//span[@id='customer.address.city.errors']");
    public By stateError = By.xpath("//span[@id='customer.address.state.errors']");
    public By zipCodeError = By.xpath("//span[@id='customer.address.zipCode.errors']");
    public By securityNumberError = By.xpath("//span[@id='customer.address.ssn.errors']");
    public By userNameError = By.xpath("//span[@id='customer.username.errors']");
    public By passwordError = By.xpath("//span[@id='customer.password.errors']");
    public By repeatedPasswordError = By.xpath("//span[@id='repeatedPassword.errors']");

    public By successful = By.xpath("//*[@class='title']");
    public By logoutButton = By.xpath("//div[@id='bodyPanel']//ul/li/a[@href='/parabank/logout.htm']");
    public RegisterPage(WebDriver driver) {
        this.driver = driver;
    }

    public void enterFirstName(String firstName) {
        driver.findElement(this.firstName).sendKeys(firstName);
    }
    public void enterLastName(String lastName) {
        driver.findElement(this.lastName).sendKeys(lastName);
    }
    public void enterAddress(String address) {
        driver.findElement(this.address).sendKeys(address);
    }
    public void enterCity(String city) {
        driver.findElement(this.city).sendKeys(city);
    }
    public void enterState(String state) {
        driver.findElement(this.state).sendKeys(state);
    }
    public void enterZipcode(String zipcode) {
        driver.findElement(this.zipCode).sendKeys(zipcode);
    }
    public void enterPhoneNumber(String phone) {
        driver.findElement(this.phone).sendKeys(phone);
    }
    public void enterSecurityNumber(String securityNumber) {
        driver.findElement(this.securityNumber).sendKeys(securityNumber);
    }
    public void enterUsername(String username) {
        driver.findElement(this.userName).sendKeys(username);
    }
    public void enterPassword(String password) {
        driver.findElement(this.password).sendKeys(password);
    }
    public void enterPasswordConfirmation(String passwordConfirmation) {
        driver.findElement(this.passwordConfirmation).sendKeys(passwordConfirmation);
    }
    public void clickOnRegisterButton() {
        driver.findElement(register).click();
    }
    public String getFirstNameError() {
        return driver.findElement(firsNameError).getText();
    }
    public String getConfirmationPasswordError() {
        return driver.findElement(repeatedPasswordError).getText();
    }
    public String successfulMessageIsDisplayed() {
        return driver.findElement(successful).getText();
    }
    public boolean logoutButtonIsDisplayed() {
        return driver.findElement(logoutButton).isDisplayed();
    }
    public String getUserNameError() {
        return driver.findElement(userNameError).getText();
    }
}
