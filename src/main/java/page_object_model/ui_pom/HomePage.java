package page_object_model.ui_pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage {

    private final WebDriver driver;


    public static String homePageURL = "https://parabank.parasoft.com/parabank/index.htm";

    public By registerLink = By.xpath("//div[@id='loginPanel']/p[2]/a");
    public By userName = By.xpath("//input[@type='text']");
    public By password = By.xpath("//input[@type='password']");
    public By loginButton = By.xpath("//input[@type='submit']");

    public HomePage(WebDriver driver) {
        this.driver = driver;
    }

    public void clickOnRegisterLink() {
        driver.findElement(registerLink).click();

    }
    public void enterUserName(String userName) {
        driver.findElement(this.userName).sendKeys(userName);
    }

    public void enterPassword(String password) {
        driver.findElement(this.password).sendKeys(password);
    }
    public void clickOnLoginButton() {
        driver.findElement(loginButton).click();
    }
}
