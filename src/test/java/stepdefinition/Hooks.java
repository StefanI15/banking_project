package stepdefinition;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Hooks {

    protected static WebDriver driver;

    @Before
    public void setUp() throws IOException {
        System.out.println("Setup method");
        InputStream input = new FileInputStream("src/test/resources/default.properties");
        Properties properties = new Properties();
        properties.load(input);

        String browser =properties.getProperty("browser");
        driver = HooksDriverFactory.getDriver(browser);
    }

    @After
    public void tearDown() {
        System.out.println("Cleanup method");
        driver.quit();
    }
}
