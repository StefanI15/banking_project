package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page_object_model.ui_pom.AccountOverviewPage;
import page_object_model.ui_pom.HomePage;

import static stepdefinition.Hooks.driver;

public class HomeSteps {

    HomePage homePage;
    AccountOverviewPage accountOverviewPage;

    RegisterSteps registerSteps;

    @Given("be on home page")
    public void beOnHomePage() {
        driver.get(HomePage.homePageURL);
        homePage = new HomePage(driver);
    }

    @When("click on register link")
    public void clickOnRegisterButton() {
        homePage.clickOnRegisterLink();
    }

/*    @Then("register page is displayed")
    public void registerPageIsDisplayed() {
        RegisterPage registerPage = new RegisterPage(driver);
        Assert.assertEquals("Register page is not displayed", "Signing up is easy!",
                registerPage.getSuccessfulMessage());
    }*/
    @Then("register page is displayed")
    public void registerPageIsDisplayed() {
        Assert.assertEquals("Register page is not displayed", "ParaBank | Register for Free Online Account Access",
                driver.getTitle());
    }

    @When("enter username {string}")
    public void enterUsername(String userName) {
        homePage.enterUserName(userName);
    }

    @And("enter password {string}")
    public void enterPassword(String password) {
        homePage.enterPassword(password);
    }

    @And("click on login button")
    public void clickOnLoginButton() {
        homePage.clickOnLoginButton();
    }

    @Then("account overview page is displayed")
    public void accountOverviewPageIsDisplayed() {
        System.out.println("Account Overview page is displayed");
        accountOverviewPage = new AccountOverviewPage(driver);
        Assert.assertEquals("The URL is not correct", "https://parabank.parasoft.com/parabank/overview.htm",
                accountOverviewPage.getAccountOverviewURl());

    }

    @And("logout button is displayed")
    public void logoutButtonIsDisplayed() {
        System.out.println("Logout button is displayed");
        accountOverviewPage = new AccountOverviewPage(driver);
        Assert.assertTrue("Logout button is not displayed", accountOverviewPage.logoutButtonIsDisplayed());
    }
}
