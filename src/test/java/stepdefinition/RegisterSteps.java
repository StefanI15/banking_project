package stepdefinition;

import com.github.javafaker.Faker;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import page_object_model.ui_pom.RegisterPage;

import java.util.List;

import static stepdefinition.Hooks.driver;

public class RegisterSteps {

    RegisterPage registerPage;
    private String baseUsername = "Clay";
    private Faker faker = new Faker();
    private String dynamicUserName;

    @Given("be on register page")
    public void beOnRegisterPage() {
        driver.get(RegisterPage.registerURL);
        registerPage = new RegisterPage(driver);
    }

    @When("enter all mandatory fields except first name")
    public void enterAllMandatoryFieldsExceptFirstName(List<String> registerFields) {
        registerPage.enterLastName(registerFields.get(0));
        registerPage.enterAddress(registerFields.get(1));
        registerPage.enterCity(registerFields.get(2));
        registerPage.enterState(registerFields.get(3));
        registerPage.enterZipcode(registerFields.get(4));
        registerPage.enterSecurityNumber(registerFields.get(6));
        registerPage.enterUsername(registerFields.get(7));
        registerPage.enterPassword(registerFields.get(8));
        registerPage.enterPasswordConfirmation(registerFields.get(9));
    }

    @And("click on register button")
    public void clickOnRegisterButton() {
        System.out.println("Register button is clicked");
        registerPage.clickOnRegisterButton();
    }

    @Then("error message for first name is displayed")
    public void errorMessageForFirstNameIsDisplayed() {
        System.out.println("Error message for first name is displayed");
        Assert.assertEquals("Error message for first name is not displayed", "First name is required.",
                registerPage.getFirstNameError());
    }

    @When("enter all mandatory fields but with wrong confirmation password")
    public void enterAllMandatoryFieldsButWithWrongConfirmationPassword(List<String> registerFields) {
        registerPage.enterFirstName(registerFields.get(0));
        registerPage.enterLastName(registerFields.get(1));
        registerPage.enterAddress(registerFields.get(2));
        registerPage.enterCity(registerFields.get(3));
        registerPage.enterState(registerFields.get(4));
        registerPage.enterZipcode(registerFields.get(5));
        registerPage.enterSecurityNumber(registerFields.get(7));
        registerPage.enterUsername(registerFields.get(8));
        registerPage.enterPassword(registerFields.get(9));
        registerPage.enterPasswordConfirmation(registerFields.get(10));
    }

    @Then("error message for confirmation password is displayed")
    public void errorMessageForConfirmationPasswordIsDisplayed() {
        System.out.println("Error message for confirmation password is displayed");
        Assert.assertEquals("Error message for confirmation password is not displayed",
                "Passwords did not match.", registerPage.getConfirmationPasswordError());
    }
    String generatedUser = generateUserName();
    @When("enter all mandatory fields and correct confirmation password")
    public void enterAllMandatoryFields(List<String> registerFields) {
/*        String dynamicIdentifier = generateUserName();
        dynamicUserName = baseUsername + dynamicIdentifier;*/


        registerPage.enterFirstName(registerFields.get(0));
        registerPage.enterLastName(registerFields.get(1));
        registerPage.enterAddress(registerFields.get(2));
        registerPage.enterCity(registerFields.get(3));
        registerPage.enterState(registerFields.get(4));
        registerPage.enterZipcode(registerFields.get(5));
        registerPage.enterSecurityNumber(registerFields.get(7));
        registerPage.enterUsername(generatedUser);
        registerPage.enterPassword(registerFields.get(9));
        registerPage.enterPasswordConfirmation(registerFields.get(10));
        System.out.println(generatedUser);
    }

    public String generateUserName() {
        dynamicUserName = baseUsername + faker.number().digits(5);
        return dynamicUserName;
    }


    @Then("successful message is displayed")
    public void successfulMessageIsDisplayed() {
        System.out.println("Successful message is displayed");
        Assert.assertEquals("Successful message is not displayed", "Welcome " + dynamicUserName,
                registerPage.successfulMessageIsDisplayed());
    }

    @And("Logout button is displayed")
    public void logoutButtonIsDisplayed() {
        System.out.println("Logout button is displayed");
        Assert.assertTrue("Logout button is not displayed", registerPage.logoutButtonIsDisplayed());
    }

    @When("enter all mandatory fields but with a registered user name")
    public void enterAllMandatoryFieldsButWithARegisteredUserName(List<String> registerFields) {
        registerPage.enterFirstName(registerFields.get(0));
        registerPage.enterLastName(registerFields.get(1));
        registerPage.enterAddress(registerFields.get(2));
        registerPage.enterCity(registerFields.get(3));
        registerPage.enterState(registerFields.get(4));
        registerPage.enterZipcode(registerFields.get(5));
        registerPage.enterSecurityNumber(registerFields.get(7));
        registerPage.enterUsername(registerFields.get(8));
        registerPage.enterPassword(registerFields.get(9));
        registerPage.enterPasswordConfirmation(registerFields.get(10));
    }

    @Then("error message for user name is displayed")
    public void errorMessageForUserNameIsDisplayed() {
        System.out.println("Error message for username is displayed");
        Assert.assertEquals("Error message for username is not displayed", "This username already exists.",
                registerPage.getUserNameError());
    }
}
