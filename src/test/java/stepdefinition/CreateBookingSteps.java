package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import page_object_model.api_pom.BookingDatesForCreateBooking;
import page_object_model.api_pom.CreateBooking;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;

public class CreateBookingSteps {
    CreateBooking requestBody;

    RequestSpecification request;
    Response response;


    @Given("create request body for a book")
    public void createRequestBodyForABook() {
        requestBody = new CreateBooking("Stefan", "Arsenie", 120, false,
                new BookingDatesForCreateBooking("2024-01-01", "2024-02-01"), "Stefan api framework");
    }

    @And("set content type {string}")
    public void setContentType(String appJson) {
        request = given().contentType(appJson);
    }

    @And("set request body created")
    public void setRequestBodyCreated() {
        request.body(requestBody);
    }

    @When("execute POST request {string}")
    public void executePOSTRequest(String postEndpoint) {
        response = request.when().post(postEndpoint);
    }

    @Then("log and verify status code is {int}")
    public void logAndVerifyStatusCodeIs(int statusCode) {
        response.then().log().body().statusCode(statusCode);
    }

    @And("verify that response body has key {string} equal to {string}")
    public void verifyThatResponseBodyHasKeyEqualTo(String key, String expectedValue) {
        response.then().body(key, equalTo(expectedValue));
    }
}
