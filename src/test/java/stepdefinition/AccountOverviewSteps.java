package stepdefinition;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.junit.Assert;
import page_object_model.ui_pom.AccountOverviewPage;

import java.time.Duration;

import static stepdefinition.Hooks.driver;

public class AccountOverviewSteps {
    AccountOverviewPage accountOverviewPage;


    @Given("be on overview page")
    public void beOnOverviewPage() throws InterruptedException {
        driver.get(AccountOverviewPage.accountOverviewURL);
        accountOverviewPage = new AccountOverviewPage(driver);
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(2));
    }

    @Then("verify that account number is correct")
    public void verifyThatAccountNumberBalanceAvailableAmountAndTotalAreCorrect() {
        System.out.println("The account number is correct");
        Assert.assertEquals("The account number is not correct", "14454", accountOverviewPage.getAccountNumber());
    }

    @And("verify that balance is correct")
    public void verifyThatBalanceIsCorrect() {
        String balance = "$3500,000.00";
        System.out.println("The balance at start is $3500,000.00");
        Assert.assertEquals("The balance is not $3500,000.00", balance, accountOverviewPage.getBalance());
    }

    @And("verify that available amount is correct")
    public void verifyThatAvailableAmountIsCorrect() {
        String availableAmount = "$3500,000.00";
        System.out.println("The available amount at start is $3500,000.50");
        Assert.assertEquals("The balance is not $3500,000.00", availableAmount, accountOverviewPage.getAvailableAmount());

    }

    @And("verify that total is correct")
    public void verifyThatTotalIsCorrect() {
        String total = "$3500,000.00";
        System.out.println("The total at start is $3500,000.00");
        Assert.assertEquals("The balance is not $3500,000.00", total, accountOverviewPage.getTotal());
    }
}
