@API
  Feature: Booking test scenarios

    Scenario: Create a booking
      Given create request body for a book
      And set content type "application/json"
      And set request body created

      When execute POST request "https://restful-booker.herokuapp.com/booking"
      Then log and verify status code is 200
      And verify that response body has key "booking.additionalneeds" equal to "Stefan api framework"