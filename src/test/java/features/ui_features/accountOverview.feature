@UI @Positive
  Feature: Account Overview test scenarios
    Background: Go to Overview page
      Given be on home page
      When enter username "Clay06737"
      When enter password "clay123"
      When click on login button
    Scenario: Verify account number, balance, available amount and total

      When be on overview page

      Then verify that account number is correct
      And verify that balance is correct
      And verify that available amount is correct
      And verify that total is correct

