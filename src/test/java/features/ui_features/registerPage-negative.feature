@UI @Negative
Feature: Register page negative test scenarios

  Scenario: Register with all mandatory fields except first name
    Given be on register page

    When enter all mandatory fields except first name

      | Arsenie         |
      | 4 Halifax Court |
      | Los Angeles     |
      | California      |
      | 90003           |
      | 123123123       |
      | 888888          |
      | George          |
      | ChatBot123      |
      | ChatBot123      |
    And click on register button
    Then error message for first name is displayed

  Scenario: Register with mandatory fields but with wrong confirmation password
    Given be on register page

    When enter all mandatory fields but with wrong confirmation password
      | Stefan          |
      | Arsenie         |
      | 4 Halifax Court |
      | Los Angeles     |
      | California      |
      | 90003           |
      | 123123123       |
      | 888888          |
      | George          |
      | ChatBot123      |
      | ChatBot12       |
    And click on register button
    Then error message for confirmation password is displayed
  @Smoke
  Scenario: Register with all mandatory fields but with a user name already registered
    Given be on register page

    When enter all mandatory fields but with a registered user name
      | Stefan          |
      | Arsenie         |
      | 4 Halifax Court |
      | Los Angeles     |
      | California      |
      | 90003           |
      | 123123123       |
      | 888888          |
      | Clay94886       |
      | clay123         |
      | clay123         |
    And click on register button
    Then error message for user name is displayed
