@UI
  Feature: Home page test scenarios
    @Smoke
    Scenario: Verify that register button goes to register page
      Given be on home page

      When click on register link
      Then register page is displayed
    @Smoke @Login @Positive
    Scenario: Login by entering correct username and password
      Given be on home page

      When enter username "Clay15695"
      And enter password "clay123"
      And click on login button
      Then account overview page is displayed
      And logout button is displayed