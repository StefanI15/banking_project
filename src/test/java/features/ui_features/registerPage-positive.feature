@UI @Positive
Feature: Register page positive test scenarios

  Scenario: Register with all mandatory fields
    Given be on register page

    When enter all mandatory fields and correct confirmation password
      | Stefan            |
      | Arsenie           |
      | 4 Halifax Court   |
      | Los Angeles       |
      | California        |
      | 90003             |
      | 123123123         |
      | 888888            |
      | <dynamicUserName> |
      | clay123           |
      | clay123           |
    And click on register button
    Then successful message is displayed
    And Logout button is displayed